/**
   The Employee class stores data about an employee
   for the Employee and ProductionWorker Classes
   programming challenge.
*/

public class Employee
{//Start of class
   private String name;             // Employee name
   private String employeeNumber;   // Employee number
   private String hireDate;         // Employee hire date
   
   /**
      This constructor initializes an object with a name,
      employee number, and hire date.
      @param n The employee's name.
      @param num The employee's number.
      @param date The employee's hire date.
   */
      
   public Employee(String n, String num, String date) throws InvalidEmployeeNumber
   {//Start of constructor
      name = n;
      setEmployeeNumber(num);
      hireDate = date;
   }//End of constructor

   /**
      The no-arg constructor initializes an object with
      null strings for name, employee number, and hire
      date.
   */
      
   public Employee()
   {//Start of constructor
      name = "";
      employeeNumber = "";
      hireDate = "";
   }//End of constructor
   
   /**
      The setName method sets the employee's name.
      @param n The employee's name.
   */

   public void setName(String n)
   {//Start of method
      name = n;
   }//End of method

   /**
      The setEmployeeNumber method sets the employee's
      number.
      @param e The employee's number.
   */

   public void setEmployeeNumber(String e) throws InvalidEmployeeNumber
   {//Start of method
      if (isValidEmpNum(e))
         employeeNumber = e;
      else
         throw new InvalidEmployeeNumber();
   }//End of method

   /**
      The setHireDate method sets the employee's
      hire date.
      @param h The employee's hire date.
   */

   public void setHireDate(String h)
   {//Start of method
      hireDate = h;
   }//End of method

   /**
      The getName method returns the employee's name.
      @return The employee's name.
   */

   public String getName()
   {//Start of method
      return name;
   }//End of method

   /**
      The getEmployeeNumber method returns the
      employee's number.
      @return The employee's number.
   */

   public String getEmployeeNumber()
   {//Start of method
      return employeeNumber;
   }//End of method

   /**
      The getHireDate method returns the
      employee's hire date.
      @return The employee's hire date.
   */
   
   public String getHireDate()
   {//Start of method
      return hireDate;
   }//End of method

   /**
      isValidEmpNum is a private method that
      determines whether a string is a valid
      employee number.
      @param e The string containing an employee
             number.
      @return true if e references a valid ID number,
              false otherwise.
   */

   private boolean isValidEmpNum(String e)
   {//Start of method
      boolean status = true;
      
      if (e.length() != 5)
         status = false;
      else
      {
         if ((!Character.isDigit(e.charAt(0))) ||
             (!Character.isDigit(e.charAt(1))) ||
             (!Character.isDigit(e.charAt(2))) ||
             (e.charAt(3) != '-')              ||
             (e.charAt(4) < 'A')               ||
             (e.charAt(4) > 'M')  )
            status = false;
      }
      
      return status;
   }//End of method

   /**
      toString method
      @return A reference to a String representation of
              the object.
   */
   
   public String toString()
   {//Start of method
      String str = "Name: " + name + "\nEmployee Number: ";
      
      if (employeeNumber == "")
         str += "INVALID EMPLOYEE NUMBER";
      else
         str += employeeNumber;
         
      str += ("\nHire Date: " + hireDate);
      return str;
   }//End of method
   
}//End of class