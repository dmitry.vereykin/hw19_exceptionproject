/**
 * Created by Dmitry on 7/8/2015.
 */
public class InvalidShift extends Exception {
    public InvalidShift() {
        super("ERROR: Invalid shift number. ");
    }
}
